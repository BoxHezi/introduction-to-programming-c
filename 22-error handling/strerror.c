#include <stdio.h>
#include <errno.h>
#include <string.h>

int main()
{
    FILE *fp;
    fp = fopen("NOT EXIST.txt", "r");

    printf("Error is: %d\n", errno);
    printf("The error message is: %s\n", strerror(errno));
    return 0;
}