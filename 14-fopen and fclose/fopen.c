#include <stdio.h>

int main()
{
    char file[] = "myfile.txt";
    FILE * fp;
    fp = fopen(file, "r+");

    if (fp == NULL)
    {
        fprintf(stderr, "Unable to open file %s\n", file);
    }
    fclose(fp);
    return 0;
}