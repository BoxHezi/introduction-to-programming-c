#include <stdio.h>

int main()
{
    char file[] = "newfile.txt";
    FILE * fp;
    fp = fopen(file, "w");
    if (fp == NULL)
    {
        fprintf(stderr, "Unable to open file %s\n", file);
    }
    fclose(fp);
    return 0;
}