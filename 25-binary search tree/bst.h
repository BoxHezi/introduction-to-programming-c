
typedef struct node {
    int data;
    struct node *left, *right;
}Node;

typedef struct tree {
    Node *root;
    unsigned size;
}Tree;

Tree *initTree();
Tree *insert(Tree *bst, int value);