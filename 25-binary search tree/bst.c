#include <stdio.h>
#include <stdlib.h>
#include "bst.h"

void inorder(Node *node)
{
    if (node != NULL)
    {
        inorder(node->left);
        printf("%d \n", node->data);
        inorder(node->right);
    }
}

int main()
{
    Tree *bst = initTree();
    insert(bst, 50);
    insert(bst, 30);
    insert(bst, 20);
    insert(bst, 40);
    insert(bst, 70);
    insert(bst, 60);
    insert(bst, 80);
    inorder(bst->root);
    printf("%d\n", bst->size);
    return 0;
}

Tree *initTree()
{
    Tree *bst = malloc(sizeof(Tree));
    bst->root = NULL;
    bst->size = 0;
    return bst;
}

Node *createNode(int value)
{
    Node *node = malloc(sizeof(Node));
    node->data = value;
    node->left = NULL;
    node->right = NULL;
    return node;
}

Tree *insert(Tree *bst, int value)
{
    Node *newNode = createNode(value);
    Node *tempNode = NULL;
    if (bst->root == NULL)
    {
        bst->root = newNode;
        bst->size++;
        return bst;
    }
    else
    {
        tempNode = bst->root;
        while (1)
        {
            if (value < tempNode->data)
            {
                if (tempNode->left == NULL)
                {
                    tempNode->left = newNode;
                    break;
                }
                tempNode = tempNode->left;
            }
            else if (value > tempNode->data)
            {
                if (tempNode->right == NULL)
                {
                    tempNode->right = newNode;
                    break;
                }
                tempNode = tempNode->right;
            }
        }
        bst->size++;
        return bst;
    }
}