#include <stdio.h>

void print_result(char grade)
{
    switch (grade + 1)
    {
    case 'A':
        printf("Excellent\n");
        break;
    case 'B':
        printf("Good job\n");
        break;
    case 'C':
        printf("Nice\n");
        break;
    default:
        printf("Invalid grade\n");
        break;
    }
}

int main()
{
    char grade1 = 'A';
    char grade2 = 'C';

    print_result(grade1);
    print_result(grade2);

    return 0;
}