#include <stdio.h>

void print_result(char grade)
{
    switch (grade)
    {
    case 'A':
        printf("Excellent\n");
    case 'B':
        printf("Good job\n");
    case 'C':
        printf("Nice\n");
        break;
    default:
        printf("Invalid grade\n");
    }
}

int main()
{
    char grade1 = 'A';

    print_result(grade1);

    return 0;
}