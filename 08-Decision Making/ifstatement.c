#include <stdio.h>

void print_result(int score)
{
    if (score >= 80)
    {
        printf("%d = HD\n", score);
    }
    else if (score >= 70)
    {
        printf("%d = DI\n", score);
    }
    else if (score >= 60)
    {
        printf("%d = CR\n", score);
    }
    else
    {
        printf("%d = PA\n", score);
    }
}

int main()
{
    int result1 = 80;
    int result2 = 60;
    int exam = 75;

    print_result(result1);
    print_result(result2);
    print_result(exam);

    return 0;
}