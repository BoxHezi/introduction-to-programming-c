#include <stdio.h>

int main()
{
    int a = 0;
    int i = 100;
    while (i)
    {
        if (i > 5 )
        {
            i--;
            continue;
            printf("This part will never be reached!\n");
        }

        printf("Value of a is %d\n", a);
        a++;
        if (a >= 5)
        {
            break;
        }
    }
    
    return 0;
}