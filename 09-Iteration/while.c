#include <stdio.h>

int main()
{
    int a = 0;
    int i = 5;

    while (i)
    {
        printf("Value a is: %d\n", a);
        a++;
        i--;
    }

    return 0;
}