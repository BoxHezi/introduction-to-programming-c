#include <stdio.h>

int main(void)
{
    int a = 9;
    int * ptr_a;
    
    ptr_a = &a;

    printf("Memory address of a: %p\n", ptr_a);
    printf("Value a: %d\n", *ptr_a);

    int b = *ptr_a + 5;
    printf("Memory address of b: %p\n", &b);
    printf("Value b: %d\n", b);
    return 0;
}
