#include <stdio.h>

void readRestOfLine()
{
    int ch;
    while (ch = getc(stdin), ch != EOF && ch != '\n') {}
    clearerr(stdin);
}

int main()
{
    char str[3];
    while (fgets(str, sizeof(str), stdin) != NULL)
    {
        printf("Input: ");
        printf("You entered: %s\n", str);
        readRestOfLine();
    }
    return 0;
}