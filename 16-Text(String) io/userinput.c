#include <stdio.h>
#define EXTRA_SPACE 2

int main()
{
    char userInput[20 + EXTRA_SPACE];
    printf("Please enter your name: ");
    fgets(userInput, sizeof(userInput), stdin);
    printf("Hello %s", userInput);
    return 0;
}