#include <stdio.h>

int main()
{
    char filename[] = "input2.txt";
    char buffer[32];
    FILE *fp;
    fp = fopen(filename, "r");
    while(fgets(buffer, sizeof(buffer), fp) != NULL)
    {
        printf("%s", buffer);
    }
    printf("\n%s\n", buffer);
    fclose(fp);
    return 0;
}