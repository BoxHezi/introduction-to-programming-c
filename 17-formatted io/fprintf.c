#include <stdio.h>

int main()
{
    char filename[] = "output.txt";
    FILE *fp;
    int i = 10;
    fp = fopen(filename, "w+");
    fprintf(fp, "Hello from fprintf() and value i is %d\n", i);
    return 0;
}