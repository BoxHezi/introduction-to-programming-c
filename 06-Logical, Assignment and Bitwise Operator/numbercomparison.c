#include <stdio.h>

int main()
{
    int a = 1;
    int b = 2;
    int c = 1;

    if (a == b)
    {
        printf("a equals to b\n");
    } 
    else
    {
        printf("a does not equal to b\n");
    }

    if (a == c)
    {
        printf("a equals to c\n");
    }
    else
    {
        printf("a does not equal to c\n");
    }
    
    return 0;
}