#include <stdio.h>

void and(int a, int b);
void or (int a, int b);

int main()
{
    int a = 10;
    int b = 5;
    int c = 0;

    and(a, b);
    and(a, c);
    or(a, b);
    or(a, c);

    return 0;
}

void and(int a, int b)
{
    if (a && b)
    {
        printf("True\n");
    }
    else
    {
        printf("False\n");
    }
}

void or(int a, int b)
{
    if (a || b)
    {
        printf("True\n");
    }
    else
    {
        printf("False\n");
    }
}
