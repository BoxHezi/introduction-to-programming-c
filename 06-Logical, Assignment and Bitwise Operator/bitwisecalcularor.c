#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "bitwisecalcularor.h"

#define EXTRA_SPACES 2

void readRestOfLine()
{
    int ch;
    while(ch = getc(stdin), ch != EOF && ch != '\n')
    { }
    clearerr(stdin);
}

int main()
{
    char optionInput[1 + EXTRA_SPACES];
    int option = 0;

    int firstNum, secondNum, resultNum;
    int firstNumArray[8], secondNumArray[8], resultNumArray[8];


    do
    {
        menu();
        fgets(optionInput, sizeof(optionInput), stdin);

        if (optionInput[strlen(optionInput) - 1] != '\n')
        {
            printf("Invalid option!\n");
            readRestOfLine();
        }
        else
        {
            option = strtol(optionInput, NULL, 10);
            if (option == 1)
            {
                printf("You selected & operator\n");
                break;
            }
            else if (option == 2)
            {
                printf("You selected | operator\n");
                break;
            }
            else if (option == 3)
            {
                printf("You selected ^ operator\n");
                break;
            }
            else
            {
                printf("Invalid option!\n");
            }

            optionInput[strlen(optionInput) - 1] = '\n';
        }

    } while (!0);

    firstNum = get_num();
    secondNum = get_num();
    if (option == 1)
    {
        resultNum = firstNum & secondNum;
    }
    else if (option == 2)
    {
        resultNum = firstNum | secondNum;
    }
    else
    {
        resultNum = firstNum ^ secondNum;
    }

    printf("Result is: %d\n", resultNum);
    to_bin(firstNum, firstNumArray);
    to_bin(secondNum, secondNumArray);
    to_bin(resultNum, resultNumArray);
    

    return 0;
}

int get_num()
{
    char numInput[3 + EXTRA_SPACES];
    int num = 0;

    do
    {
        printf("Please enter a number: ");
        fgets(numInput, sizeof(numInput), stdin);

        if (numInput[strlen(numInput) - 1] != '\n')
        {
            printf("Invalid input!\n");
            readRestOfLine();
        }
        else
        {
            num = strtol(numInput, NULL, 10);
            if (num > 255)
            {
                printf("The largest number support is 255!\n");
            }
            else
            {
                return num;
            }
        }
    } while (!0);
    
}

int * to_bin(unsigned int a, int * numArray)
{
    int counter = 7;
    int bit = 0;
    int i;

    while (counter >= 0)
    {
        bit = a % 2;
        a /= 2;
        *(numArray + counter) = bit;
        counter--;
    }

    for (i = 0; i < 8; i++)
    {
        printf("%d", *(numArray + i));
    }
    printf("\n");

    return numArray;
}

void menu()
{
    printf("Welcome to bitwise operator!\n");
    printf("============================\n");
    printf("Please select an option:\n");
    printf("1. &\n2. |\n3. ^\n");
    printf("Please enter your option: ");
}