#include <stdio.h>

typedef enum boolean {
    FALSE = 0,
    TRUE = 1
}Boolean;

int main()
{
    Boolean b = FALSE;
    printf("Boolean value is: %d\n", b);
    return 0;
}