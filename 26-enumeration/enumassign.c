#include <stdio.h>

enum constant{zero, one, five = 5, six};

int main()
{
    enum constant c0 = zero, c5 = five, c6 = six;
    printf("c0: %d, c5: %d, c6: %d\n", c0, c5, c6);
    return 0;
}