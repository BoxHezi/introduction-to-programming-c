#include <stdio.h>

int main()
{
    int x[5] = {1, 2, 3, 4, 5};
    int * ptr;
    int i;

    ptr = &x[0];

    for (i = 0; i < 5; i++)
    {
        printf("*ptr + %d = %d\n", i, *ptr+i);
        printf("x[%d] = %d\n", i, x[i]);
    }

    return 0;
}