#include <stdio.h>

int main()
{
    int x[5];
    int i;

    for (i = 0; i < 5; i++)
    {
        x[i] = i;
    }

    for (i = 0; i < 6; i++)
    {
        printf("%d\n", x[i]);
    }

    return 0;
}