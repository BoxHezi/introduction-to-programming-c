#include <stdio.h>

int main()
{
    char string[15] = "hello world";
    char * ptr;

    ptr = &string[0];

    while (*ptr != '\0')
    {
        printf("%c", *ptr++);
        /* ptr++; */
    }
    printf("\n");
    
    return 0;
}