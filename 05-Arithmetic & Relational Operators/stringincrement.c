#include <stdio.h>

int main()
{
    char string[] = "hello";
    printf("string is: %s\n", string);
    printf("pointer value increase by one!\n");
    string++;
    printf("string is: %s\n", string);
    return 0;
}