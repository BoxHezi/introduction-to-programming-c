Powershell Empire

Goal:  get highest level of privileges
    Domain Admin(DA)
    Enterprise Admin(EA)

Obtaining foothold
    phishing
    NULL Sessions
    LLMNR/NBT-NS Poisoning
    SMB Relay

Domain Enumeration

More about AD:
    Users
    Groups
    Machines
    Logged in users & active session 
    Local admins 
    Domain trush releationship

Tools:
    Bloodhound
    procdump.exe

run "ingestor" to enumerate the domain information:
    BloodHound.ps1 - powershell implementation
    ShardHound.exe - .NET implementation

Privilege Escalation
    Exploiting Group Policy Preferences
    LSASS - Local Security Authority Subsystem Service
    Kerberoasting
    ACL - access control list

Lateral Movement
    Between hosts on the network
        pass the hash
        NetNTLMv2
        PsExec
        WMI - Windows Management Instrumentation
    DCSync
    Between Domain:
        SID (Security Identifier)
        Machine and domain SIDs consist of a base SID and a Relative ID (RID) that is appended to the base SID
        RID LIST:
            500 - admins
            512 - Domain admins
            518 - schema Admins
            519 - Enterprise Admins
            520 - Group Policy Creator Owners

Persistence - Golden Ticket
