#include <stdio.h>

void readRestOfLine()
{
    int ch;
    while (ch = getc(stdin), ch != EOF && ch != '\n')
    {}
    clearerr(stdin);
}

int main()
{
    int c, ch;
    printf("Enter a character: ");
    c = fgetc(stdin);
    printf("Your entered: %c\n", c);
    
    readRestOfLine();

    printf("Enter a character: ");
    ch = getchar();
    printf("Your entered: %c\n", ch);
    return 0;
}