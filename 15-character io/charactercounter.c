#include <stdio.h>

int main()
{
    char fileName[] = "ActiveDirectory2.txt";
    FILE *fp;
    int ch, counter;

    fp = fopen(fileName, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "ERROR Opening file\n");
    }
    else
    {
        do {
            ch = getc(fp);
            if (ch == 'a')
            {
                counter++;
            }
        } while (ch != EOF);
        fclose(fp);
        printf("a appears %d times in the file\n", counter);
    }
    return 0;
}