#include <stdio.h>

int main()
{
    char fileName[] = "input.txt";
    FILE *fp;
    int c;
    fp = fopen(fileName, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "Error opening file\n");
    }
    else
    {
        do {
            c = fgetc(fp);
            printf("%c", c);
        } while (c != EOF);
        fclose(fp);
    }
    return 0;
}