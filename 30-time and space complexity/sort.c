#include <stdio.h>
#include <time.h>
#include "algorithms/bubble_sort.h"
#include "algorithms/insertion_sort.h"

int main()
{
    clock_t startTime, endTime, diffTime;
    int arr[] = {12, 11, 13, 5, 7, 32, 14, 53, 85, 24, 46, 18, 132};
    int arr2[] = {12, 11, 13, 5, 7, 32, 14, 53, 85, 24, 46, 18, 132};
    int size = sizeof(arr) / sizeof(arr[0]);

    startTime = clock();
    bubble_sort(arr, size);
    endTime = clock();
    diffTime = endTime - startTime;
    printf("Time taken: %ld\n", diffTime);

    startTime = clock();
    insertion_sort(arr2, size);
    endTime = clock();
    diffTime = endTime - startTime;
    printf("Time taken: %ld\n", diffTime);

    return 0;
}