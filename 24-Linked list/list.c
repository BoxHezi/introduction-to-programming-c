#include "list.h"

int main()
{
    Link *link = initLinkedList();
    insert(link, 10);
    insert(link, 20);
    if (link->head == NULL)
    {
        printf("NULL\n");
    }
    else
    {
        printf("Linked List size is: %d\n", link->size);
    }
    printLink(link);
    return 0;
}

void printLink(Link *link)
{
    Node *node = link->head;
    int i;
    for (i = 0; i < link->size; i++)
    {
        printf("List index: %d, data is: %d\n", i, node->data);
        node = node->next;
    }
}

Link *initLinkedList()
{
    Link *link = malloc(sizeof(Link));
    if (link == NULL)
    {
        exit(EXIT_FAILURE);
    }
    else
    {
        link->head = NULL;
        link->size = 0;
    }
    return link;
}

Node *createNode(int data)
{
    Node *newNode = (Node *)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    return newNode;
}

void insert(Link *link, int data)
{
    Node *newNode = createNode(data);
    Node *tempNode = NULL;
    if (link->head == NULL)
    {
        link->head = newNode;
    }
    else
    {
        tempNode = link->head;
        while (1)
        {
            if (tempNode->next == NULL)
            {
                tempNode->next = newNode;
                break;
            }
            tempNode = tempNode->next;
        }
    }

    link->size++;
}