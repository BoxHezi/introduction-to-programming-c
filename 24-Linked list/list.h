#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int data;
    struct node * next;
} Node;

typedef struct link {
    Node *head;
    unsigned size;
} Link;

void insert(Link *link, int data);
Link *initLinkedList();
void printLink(Link *link);