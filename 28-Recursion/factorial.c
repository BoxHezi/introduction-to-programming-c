#include <stdio.h>
#include <stdlib.h>

long int factorial(unsigned int x)
{
    if (x <= 1)
    {
        return 1;
    }
    return x * factorial(x - 1);
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Invalid argument!\n");
        return EXIT_FAILURE;
    }
    else
    {
        printf("Factorial of %s is: %ld", argv[1], factorial(atol(argv[1])));
        return EXIT_SUCCESS;
    }
}