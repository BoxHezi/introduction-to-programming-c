int recurFunc(int x) 
{
    if (x == 0)
    {
        return;
    }
    recurFunc(x - 1);
}