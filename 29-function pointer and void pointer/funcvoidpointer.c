#include <stdio.h>

int add(int x, int y)
{
    printf("Addition result: %d\n", (x + y));
    return x + y;
}

int substract(int x, int y)
{
    printf("Substraction result: %d\n", (x - y));
    return x - y;
}

int time(int x, int y)
{
    printf("Multiplication result: %d\n", (x * y));
    return x * y;
}

int main()
{
    int (*ptr[])(int, int) = {add, substract, time};
    unsigned a = 9, b = 4;
    int i;
    for (i = 0; i < 3; i++)
    {
        (*ptr[i])(a, b);
    }
    return 0;
}
