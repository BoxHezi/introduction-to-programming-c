#include <stdio.h>

int main()
{
    int a = 1;
    char b = 'b';
    void *ptr;
    ptr = &a;
    printf("%p has value: %d\n", ptr, *(int *)ptr);
    ptr = &b;
    printf("%p has value: %c\n", ptr, *(char *)ptr);
    return 0;
}