#include <stdio.h>

int addition(int a, int b)
{
    return a + b;
}

int main()
{
    int (*func_pointer)(int, int);
    int result;
    func_pointer = addition;
    result = func_pointer(10, 20);
    printf("Result is %d\n", result);
    return 0;
}