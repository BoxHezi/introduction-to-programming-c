#include <stdio.h>
#include <string.h>

typedef struct Student
{
    char studentNo[5];
    char name[20];
    int ass1, ass2;
    float total;
    char grade[2];
} Student;

void printDetail(Student s)
{
    printf("Student No: %s\n", s.studentNo);
}

int main()
{
    Student s1;

    strcpy(s1.studentNo, "S001");
    strcpy(s1.name, "Student one");
    s1.ass1 = 80;
    s1.ass2 = 90;
    s1.total = (s1.ass1 + s1.ass2) / 2;
    strcpy(s1.grade, "HD");

    printDetail(s1);
    return 0;
}