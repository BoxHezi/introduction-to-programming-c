#include <stdio.h>

typedef enum boolean
{
    FALSE,
    TRUE
} Boolean;

int main()
{
    Boolean b = TRUE;

    while (b)
    {
        printf("VALUE OF TRUE IS: %d\n", b);
        printf("VALUE OF FALSE IS: %d\n", !b);
        b = !b;
    }
    return 0;
}
