#include <stdio.h>

void print()
{
    int a = 0;
    static int b = 0;
    printf("a: %d, b: %d\n", a, b);
    a++;
    b++;
}

int main()
{
    int i = 0;
    for (i = 0; i < 15; i++)
    {
        print();
    }
    return 0;
}