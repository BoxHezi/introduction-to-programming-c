#include <stdio.h>

void static_var()
{
    static int a = 10;
}

int main()
{
    static_var();
    printf("a is: %d\n", a);
    return 0;
}