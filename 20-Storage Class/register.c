#include <stdio.h>

int main()
{
    register int a;
    printf("a: %d\n", a);
    printf("Address of a: %u\n", &a);
    return 0;
}