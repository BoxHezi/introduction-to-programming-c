#include <stdio.h>

int main(void)
{
    printf("==========a==========\n");
    char char_a = 'a';
    printf("%c=%d\n", char_a, char_a);

    printf("\n");
    printf("==========A==========\n");
    char char_A = 'A';
    printf("%c=%d\n", char_A, char_A);
    return 0;
}