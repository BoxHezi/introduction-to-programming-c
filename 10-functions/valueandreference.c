#include <stdio.h>

void max(int a, int b, int *max_num)
{
    if (a > b)
    {
        *max_num = a;
    }
    else
    {
        *max_num = b;
    }
}

int main()
{
    int a = 10, b = 5, max_num = 0;
    max(a, b, &max_num);
    printf("Max is: %d\n", max_num);
    return 0;
}