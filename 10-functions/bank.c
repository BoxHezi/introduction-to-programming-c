#include <stdio.h>

float balance(char customer)
{
    switch (customer)
    {
        case 'a':
            printf("You have $100\n");
            return 100;
        case 'b':
            printf("You have $500\n");
            return 500;
        default:
            printf("Invalid account\n");
            return 0;
    }
}

int main()
{
    char customer1 = 'a';
    char customer2 = 'b';
    char customer3 = 'c';

    balance(customer1);
    balance(customer2);
    balance(customer3);

    return 0;
}