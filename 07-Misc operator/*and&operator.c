#include <stdio.h>

int main()
{
    int a = 10;
    int * ptr;

    ptr = &a; /* get address of variable a */

    printf("The address of a is: %p\n", ptr);
    printf("The address of a is: %p\n", (void *) &a);
    printf("The value of a is: %d\n", *ptr); /* dereferencing */

    return 0;
}