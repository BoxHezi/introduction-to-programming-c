#include <stdio.h>

int main()
{
    int a = 10, b = 20, c = 10;

    if (a == b)
    {
        printf("a equals to b\n");
    }
    else
    {
        printf("a does not equal to b\n");
    }

    (a == b) ? printf("a equals to b\n") : printf("a does not equal to b\n");
    (a == c) ? printf("a equals to c\n") : printf("a does not equal to c\n");

    return 0;
}