#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
    char *info;
    info = malloc(10 * sizeof(char));
    strcpy(info, "hello");
    /* info = realloc(info, 20 * sizeof(char)); */
    strcat(info, " world");
    printf("%s\n", info);
    free(info);
    return 0;
}