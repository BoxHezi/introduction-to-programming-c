#define max(a, b)  (a > b? a : b)

int main()
{
    int a, b;
    a = max(10, 5);
    #undef max
    b = max(1, 2);
    return 0;
}