#include <stdio.h>
#define square(a) (a) * (a)

int main()
{
    printf("Square2(a) = %d\n", square(3 - 2));
    printf("Square2(a) = %d\n", (square(2) / square(2)));
    return 0;
}