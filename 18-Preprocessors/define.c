#include <stdio.h>

#define TRUE 1
#define FALSE 0

int main()
{
    printf("TRUE is: %d\n", TRUE);
    printf("FALSE is: %d\n", FALSE);
    return 0;
}