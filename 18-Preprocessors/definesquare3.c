#include <stdio.h>
#define square(a) ((a) * (a))

int main()
{
    printf("Square3(a) = %d\n", square(3 - 2));
    printf("Square3(a) = %d\n", (square(4) / square(2)));
    return 0;
}