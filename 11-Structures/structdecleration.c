#include <stdio.h>

struct Student
{
    char name[32];
    int age;
};

void printDetail(struct Student s)
{
    printf("Student %s is %d years old\n", s.name, s.age);
}

int main()
{
    struct Student s1 = {"student 1", 20};
    struct Student s2;
    s2.age = 10;

    printDetail(s1);
    printDetail(s2);

    return 0;
}