#include <stdio.h>
#include <string.h>

struct Student
{
    char studentNo[10];
    char name[32];
    int age;
    char major[32];
} s1, s2;

void printDetail(struct Student s)
{
    printf("Student No: %s\n", s.studentNo);
}

int main()
{
    struct Student s3;
    strcpy(s1.studentNo, "S001");
    strcpy(s2.studentNo, "S002");
    strcpy(s3.studentNo, "S003");
    printDetail(s1);
    printDetail(s2);
    printDetail(s3);

    return 0;
}